import { Component, OnInit } from '@angular/core';
import { MessagingService } from 'src/app/messaging/messaging.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  maxNotifs: number = 5;

  NbMessage: number = this.myService.getCountUnreadMessage();

  constructor(private myService: MessagingService) { }

  ngOnInit(): void {
  }

}
