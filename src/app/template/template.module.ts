import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagingModule } from '../messaging/messaging.module';





@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    MessagingModule 
  ]
})
export class TemplateModule { }
