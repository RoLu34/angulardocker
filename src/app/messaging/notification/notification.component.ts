import { Component, Input, Injectable, OnInit } from '@angular/core';
import { MessagingService } from '../messaging.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})

@Injectable({
  providedIn: 'root'
})

export class NotificationComponent implements OnInit {

  @Input() maxMessages: number = 0;

  count: number = 0;

  constructor(private service: MessagingService) { }
  
  ngOnInit(): void {
    this.count = this.service.getCountUnreadMessage();
    if(this.count >= this.maxMessages) this.count = this.maxMessages;
  }

}
