export interface Message {
    sender : string,
    receiver : string,
    read: boolean
}
