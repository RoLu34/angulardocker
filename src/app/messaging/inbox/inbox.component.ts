import { Component, OnInit } from '@angular/core';
import { NbListModule } from '@nebular/theme';


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  messages: { subject: string, user: string }[] = [
    { subject: 'Bonjour !', user: 'Carla Espinosa' },
    { subject: 'Facture n°2105183', user: 'Bob Kelso' },
    { subject: 'Invitation beuverie', user: 'Roger Pastis' },
    { subject: 'Voici comment gagner 100.000$ en 5 secondes !', user: 'Perry Cox' },
    { subject: 'Cheh', user: 'Ben Sullivan' },
  ];
}
