import { Injectable, Input } from '@angular/core';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  maxNotifs: number = 5
  @Input() maxMessages: number = 0;
  count: number = 0;

  constructor() { }

  private messages: Message[] = [
    {
      receiver : 'loic',
      sender : 'loic',
      read: false
    },
    {
      receiver : 'yassine',
      sender : 'loic',
      read: true
    }
  ]

  getCountUnreadMessage(): number{
    let count = 0;

    return this.messages.filter(message => !message.read).length;

  }

  getMessages(): Message[]{
    return this.messages;
  }

}
